# Integrantes Grupo 2

- Cristian Fernandez
- David Barrera
- Brayan Duque
- Rosario Parra
- Juan Ruiz

## Instrucciones (IMPORTANTES!)

1. Clonar el repositorio en alguna carpeta con el comando

    ```bash
    git clone https://Archiyopp@bitbucket.org/minticgrupo/bank_app.git
    ```

2. Entrar a la carpeta bank_app en vs code e iniciar el virtual env con el comando

    ```bash
    python -m venv env
    ```

3. Activar el virtual env con el comando `env\Scripts\activate` en windows o `source env/bin/activate` en linux.
4. Instalar los requerimientos con

    ```bash
    pip install -r requirements.txt
    ```
