from django.contrib import admin
from .models.detail import Detail
from .models.gallery import Gallery
from .models.plant import Plant
from .models.user import User
from .models.sale import Sale

admin.site.register(User)
admin.site.register(Plant)
admin.site.register(Sale)
admin.site.register(Gallery)
admin.site.register(Detail)
