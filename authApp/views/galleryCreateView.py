from rest_framework.response import Response
from rest_framework import generics, status
from django.conf import settings
from rest_framework_simplejwt.backends import TokenBackend
from authApp.models.detail import Detail
from authApp.serializers.detailSerializer import DetailSerializer
from rest_framework.permissions import IsAuthenticated


class DetailCreateView(generics.CreateAPIView):
    queryset = Detail.objects.all()
    serializer_class = DetailSerializer
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        token = request.META.get("HTTP_AUTHORIZATION")[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT["ALGORITHM"])
        valid_data = tokenBackend.decode(token, verify=False)
        if valid_data["user_id"] != request.data["user"]:
            stringResponse = {"detail": "Unauthorized Request"}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        return super().post(request, *args, **kwargs)
