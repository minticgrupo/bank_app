from rest_framework import generics
from authApp.models.plant import Plant
from authApp.serializers.plantSerializer import PlantSerializer


class PlantsListView(generics.ListAPIView):
    queryset = Plant.objects.all()
    serializer_class = PlantSerializer
