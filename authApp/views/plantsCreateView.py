from django.conf import settings
from rest_framework import status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework_simplejwt.backends import TokenBackend
from authApp.models.gallery import Gallery
from authApp.serializers.plantWriteSerializer import PlantWriteSerializer


class PlantsCreateView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        token = request.META.get("HTTP_AUTHORIZATION")[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT["ALGORITHM"])
        valid_data = tokenBackend.decode(token, verify=False)
        if valid_data["user_id"] != request.data["user"]:
            stringResponse = {"detail": "Unauthorized Request"}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)

        galleries = request.data.pop("galleries")
        serializer = PlantWriteSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        new_plant = serializer.save()
        for gallery in galleries:
            Gallery.objects.create(plant=new_plant, **gallery)
        return Response(serializer.data, status=status.HTTP_201_CREATED)
