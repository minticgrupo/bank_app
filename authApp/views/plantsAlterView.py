from django.conf import settings
from rest_framework import status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework_simplejwt.backends import TokenBackend
from authApp.serializers.plantWriteSerializer import PlantWriteSerializer
from authApp.models.plant import Plant

class PlantsAlterView(APIView):
    permission_classes = (IsAuthenticated,)


    def get(self, request, pk):
        try: 
            plant = Plant.objects.get(pk=pk)
        except Plant.DoesNotExist: 
            return Response({'mensaje': 'Planta no existe'}, status=status.HTTP_404_NOT_FOUND)
        serializer = PlantWriteSerializer(plant)
        return Response(serializer.data, status=status.HTTP_200_OK)


    def put(self, request, pk):
        token = request.META.get("HTTP_AUTHORIZATION")[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT["ALGORITHM"])
        valid_data = tokenBackend.decode(token, verify=False)
        try: 
            plant = Plant.objects.get(pk=pk)
        except Plant.DoesNotExist: 
            return Response({'mensaje': 'Planta no existe'}, status=status.HTTP_404_NOT_FOUND)
        if valid_data["user_id"] != plant.user_id:
            stringResponse = {"detail": "Unauthorized Request"}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        serializer = PlantWriteSerializer(plant, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)


    def delete(self, request, pk):
        token = request.META.get("HTTP_AUTHORIZATION")[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT["ALGORITHM"])
        valid_data = tokenBackend.decode(token, verify=False)
        try: 
            plant = Plant.objects.get(pk=pk)
        except Plant.DoesNotExist: 
            return Response({'mensaje': 'Planta no existe'}, status=status.HTTP_404_NOT_FOUND)
        if valid_data["user_id"] != plant.user_id:
            stringResponse = {"detail": "Unauthorized Request"}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        plant.delete()
        return Response({'mensaje': 'Planta Eliminada correctamente'}, status=status.HTTP_201_CREATED)