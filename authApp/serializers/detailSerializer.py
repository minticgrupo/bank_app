from rest_framework import serializers
from authApp.models.detail import Detail
from authApp.models.plant import Plant
from authApp.models.user import User


class DetailSerializer(serializers.ModelSerializer):
    sale_id = serializers.IntegerField(write_only=True)
    plant_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = Detail
        fields = "__all__"
        depth = 1

        def create(self, validated_data):
            detailInstance = Detail.objects.create(**validated_data)
            return detailInstance

        # def to_representation(self, obj):
        #     detail = Detail.objects.get(id=obj.id)
        #     plant = Plant.objects.get(id=obj.plant)
        #     return {
        #         "id": detail.id,
        #         "total": detail.total,
        #         "quantity": detail.quantity,
        #         "plant": {
        #             "id": plant.id,
        #             "name": plant.name,
        #             "description": plant.description,
        #             "stock": plant.stock,
        #             "price": plant.price,
        #             "type": plant.type,
        #             "state": plant.state,
        #         },
        #     }
