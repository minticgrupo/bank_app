from authApp.models.detail import Detail
from authApp.models.plant import Plant
from authApp.models.sale import Sale
from rest_framework import serializers
from .userSerializer import UserSerializer

from authApp.models.user import User


class SaleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sale
        fields = "__all__"

        def create(self, validated_data):
            saleInstance = Sale.objects.create(**validated_data)
            return saleInstance

        def to_representation(self, obj):
            sale = Sale.objects.get(id=obj.id)
            user = User.objects.get(id=obj.user)
            details = Detail.objects.filter(sale=obj.id).values()
            return {
                "id": sale.id,
                "total": sale.total,
                "address": sale.address,
                "state": sale.state,
                "buyer": {
                    "username": user.username,
                    "firstName": user.name,
                    "lastName": user.lastName,
                    "email": user.email,
                    "phone": user.phone,
                    "city": user.city,
                },
                "details": list(details),
            }
