from rest_framework import serializers
from authApp.models.sale import Sale
from authApp.models.user import User
from authApp.models.plant import Plant


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            "id",
            "username",
            "firstName",
            "password",
            "lastName",
            "email",
            "phone",
            "city",
            "is_active",
        ]
        extra_kwargs = {"password": {"write_only": True, "min_length": 4}}

        def create(self, validated_data):
            userInstance = User.objects.create(**validated_data)
            return userInstance

        def to_representation(self, obj):
            user = User.objects.get(id=obj.id)
            plants = Plant.objects.filter(user=obj.id).values()
            sales = Sale.objects.filter(user=obj.id).values()
            return {
                "id": user.id,
                "username": user.username,
                "firstName": user.name,
                "lastName": user.lastName,
                "city": user.city,
                "email": user.email,
                "phone": user.phone,
                "plants": list(plants),
                "sales": list(sales),
            }
