from authApp.models.gallery import Gallery
from authApp.models.plant import Plant
from rest_framework import serializers

from authApp.models.user import User
from .userSerializer import UserSerializer


class PlantSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = Plant
        fields = "__all__"
        depth = 1

        def create(self, validated_data):
            plantInstance = Plant.objects.create(**validated_data)
            return plantInstance

        # def to_representation(self, obj):
        #     plant = Plant.objects.get(id=obj.id)
        #     user = User.objects.get(id=obj.user)
        #     galleries = Gallery.objects.filter(plant=obj.id)
        #     return {
        #         "id": plant.id,
        #         "name": plant.name,
        #         "description": plant.description,
        #         "stock": plant.stock,
        #         "price": plant.price,
        #         "type": plant.type,
        #         "seller": {
        #             "username": user.username,
        #             "firstName": user.name,
        #             "lastName": user.lastName,
        #             "email": user.email,
        #             "phone": user.phone,
        #             "city": user.city,
        #         },
        #         "galleries": list(galleries),
        #     }
