from authApp.models.plant import Plant
from authApp.models.gallery import Gallery
from rest_framework import serializers


class PlantWriteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Plant
        fields = "__all__"
