from django.db import models
from .user import User


class Sale(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    address = models.CharField(max_length=200)
    state = models.CharField(max_length=30)
