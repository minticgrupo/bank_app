from django.db import models
from .user import User


class Plant(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    description = models.TextField(max_length=500)
    stock = models.IntegerField()
    price = models.IntegerField()
    type = models.CharField(max_length=50)
