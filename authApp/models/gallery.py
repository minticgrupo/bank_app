from django.db import models
from .plant import Plant


class Gallery(models.Model):
    id = models.AutoField(primary_key=True)
    plant = models.ForeignKey(Plant, on_delete=models.CASCADE)
    image = models.CharField(max_length=150)
    description = models.CharField(max_length=200)
